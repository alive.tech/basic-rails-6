# frozen_string_literal: true

# Base controller
class BaseController < ApplicationController
  before_action :authenticate_user!
end
