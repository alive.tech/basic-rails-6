# frozen_string_literal: true

# Home Controller
class HomeController < ApplicationController
  # cancancan authorization
  load_and_authorize_resource class: HomeController
end
