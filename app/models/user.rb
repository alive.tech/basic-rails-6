# frozen_string_literal: true

# User model
class User < ApplicationRecord
  extend Enumerize
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :confirmable, :lockable, :trackable

  enumerize :role, in: %i[lead manager admin], default: :lead, predicates: true

  validates :name, :email, :role, presence: true
end
