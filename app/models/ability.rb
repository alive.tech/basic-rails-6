# frozen_string_literal: true

# CanCanCan authorization class
class Ability
  include CanCan::Ability

  def initialize(user)

    alias_action :edit, :update, :to => :modify
    # what everyone cannot?
    cannot :manage, :all
    
    # what everyone can?
    can :read, HomeController

    # since here, only users
    return unless user.present?

    can :edit, User, id: user.id

    case user.role
    when 'lead'
      # what a common user can?

    when 'admin'
      # what admin can?
      can :manage, :all

      can :access, :rails_admin   # grant access to rails_admin
      can :read, :dashboard       # grant access to the dashboard

      # what admin cant?
      cannot :destroy, User, id: user.id
    end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
