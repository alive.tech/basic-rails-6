# frozen_string_literal: true

# Mailer to application context
class ApplicationMailer < ActionMailer::Base
  default from: 'from@example.com'
  layout 'mailer'
end
