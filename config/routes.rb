Rails.application.routes.draw do
  resources :lessons
  resources :classrooms
  resources :users, except: [:new, :create]

  root 'home#index'
  
  scope "auth" do
    devise_for :users
  end

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
end
