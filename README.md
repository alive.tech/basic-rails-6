# BASIC RAILS 6 PROJECT

A basic project with the most common gems already configured to save your time.

## Features
This project contains: 
- Ruby **2.7.1**
- Rails **6.0.22**
- Bootstrap **4.1**
- Devise
- Rails Admin
- CanCanCan
- Slim Rails
- Enumerize
- Responders
- Interactor Rails
- Octoicons
- Gitlab CI and CD configured 
- Pre-commit hook with husky

#### Running examples
This base project are running on heroku: [staging](https://basic-rails-6-staging.herokuapp.com) and [production](https://basic-rails-6-production.herokuapp.com)

## Getting Started

Download the project in [gitlab](https://gitlab.com/alive.tech/basic-rails-6/-/archive/master/basic-rails-6-master.zip) as a zip.

Extract the zip, enter in the folder and run:
```sh
bundle install
yarn install
rails g rename:into New-Name
```

Enter in new folder project and:
  * **Change the database name** name on **config > database.yml**
  * Search to **"projectname"** and replace with **new-project-name**.

Config new project:
```sh
bundle install
yarn install
rake db:create db:migrate db:seed
rails credentials:edit
```

config **credentials.yml.enc** file:

```yml
#it is used on production, config this to send mail.
smtp:
  user_name: user
  password: password
  domain: domain.com
  address: smtp.domain.com
  port: 587
  default_mail: not-reply@domain.com
```

Close the file to save.


Install dependecies gem:

```sh
gem install foreman
gem install mailcatcher
```

Now you must be able to start the project:
```sh
mailcatcher
foreman start
```

Everyting is ok? So let's config the repository in gitlab.

## CONFIG REPOSITORY

### INIT LOCAL REPO

```sh
git init
yarn install --force #to make husky works
git add .
git commit -m 'initial commit'
```

### CREATE NEW REPO 
Go to [gitlab](https://gitlab.com/projects/new) and create a new project. Do not push to server in gitlab yet.

```sh
git remote add origin <git url> #do not push to server in gitlab yet.
```

### CREATEE HEROKU APPS
Create two apps on [heroku](https://dashboard.heroku.com/new-app): one for **staging** and another for **production**. 
Go to [settings > CI / CD](https://gitlab.com/alive.tech/basic-rails-6/-/settings/ci_cd) in **gitlab** and add the variables bellow according to heroku apps that you created. 

* HEROKU_STAGING_APP
* HEROKU_STAGING_API_KEY
* HEROKU_PRODUCTION_APP
* HEROKU_PRODUCTION_API_KEY

The heroku **api key** do you get [here](https://dashboard.heroku.com/account).

#### CONFIG HEROKU ENVIROMENT
Go to setting on each **heroku app** and add the variables bellow:
* HOST_DOMAIN (this is the domain used in callbacks of mails)
* RAILS_MASTER_KEY (the content into config > master.key)

Save the **config > master.yml** in a safe place. (without this file is not possible to access the credentials **above**)

### SEND TO PIPELINE

```sh
git push origin master
```

Wait the pipeline and boow your project is runnig with **continuos integration** at staging. 

### SEND TO PRODUCTION
To send your project to product you just need to create a tag and send to repository.

For example:
```sh
git tag -a v0.1 -m "my version 0.1" # this command will create a tag witn last commit.  
git push origin master --tags

```
To learn more about tags access this [link](https://git-scm.com/book/en/v2/Git-Basics-Tagging)

**Enjoy your new and complete project!**

----

## The force is with you
After all change the README.md:
* Remove all above
* Fill informations bellow


About Project
======

* Ruby **version**: 2.7.1

* Rails **version**: 6.0.22

* System dependencies
  * Gems
    * foreman
    * mailcatcher

* Configuration:

* Database creation:

* Database initialization

* How to run the test suite
  `yarn test`

* How to run the lint suite
  `yarn lint`  
