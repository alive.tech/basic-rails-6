# Install sudo
apt-get update && apt-get -y install sudo
# Install node and some other deps
curl -sL https://deb.nodesource.com/setup_13.x | sudo -E bash -
apt-get update -yq
apt-get install -y nodejs 

# Install yarn
wget -q -O - https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list
apt-get update -yq
apt-get install -y yarn