# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Route', type: :routing do
  it 'when get root should route to home controller' do
    expect(get: '/').to route_to controller: 'home', action: 'index'
  end
end
