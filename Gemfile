# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.7.1'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 6.0.2', '>= 6.0.2.2'
# Use postgres as the database for Active Record
gem 'pg', '~> 1.2'
# Use Puma as the app server
gem 'puma', '~> 4.1'
# Use SCSS for stylesheets
gem 'sass-rails', '>= 6'
# Transpile app-like JavaScript. Read more: https://github.com/rails/webpacker
gem 'webpacker', '~> 4.0'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.7'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use Active Model has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# A Puma plugin with best-practices configuration and handy configuration variables for Heroku deployments.
gem 'puma-heroku'
# Devise is a flexible authentication solution for Rails based on Warden.
gem 'devise'
# slim-rails provides Slim generators for Rails 3+.
gem 'slim-rails'
# CanCanCan is an authorization library for Rails which restricts what resources a given user is allowed to access.
gem 'cancancan'
# Enumerated attributes with I18n and ActiveRecord/Mongoid/MongoMapper/Sequel support.
gem 'enumerize'
# Rails forms made easy.
gem 'simple_form'
# RailsAdmin is a Rails engine that provides an easy-to-use interface for managing your data.
gem 'rails_admin', '~> 2.0'
# A set of responders modules to dry up your Rails app.
gem 'responders'
# Interactor Rails provides Rails support for the Interactor gem.
gem 'interactor-rails', '~> 2.0'
# Octicons icons library
gem 'octicons_helper'
# This gem allows you to rename your Rails application by using a single command.
gem 'rename'

# Use Active Storage variant
# gem 'image_processing', '~> 1.2'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.4.2', require: false

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: %i[mri mingw x64_mingw]

  gem 'rspec-rails', '~> 4.0.0'

  gem 'rubocop', '~> 0.81.0', require: false
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'web-console', '>= 3.3.0'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'

  # Avoid repeating yourself, use pry-rails instead of copying the initializer to every rails project.
  gem 'pry-rails'
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '>= 2.15'
  gem 'selenium-webdriver'
  # Easy installation and use of web drivers to run system tests with browsers
  gem 'webdrivers'
end
